let connect = require("./connect");

class VehiculosModel extends connect{
    table = 'vehiculos';
}

module.exports = VehiculosModel;