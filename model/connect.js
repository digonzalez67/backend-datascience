var mysql = require('mysql');

class connect {
    connection ;
    table;
    whereSQL;
    group;
    countGroup;
    constructor (  ) {
        this.connection = mysql.createConnection({
            host     : 'localhost',
            user     : 'root',
            password : '',
            database : 'datascience'
        });
        this.whereSQL = '';
        this.group = '';
        this.countGroup = '';
        this.connection.connect();
    }
    //SELECT * FROM `hurtos` inner join (SELECT departamento.id as id_departamento , departamento.departamento , municipio.id as id_municipio , municipio.municipio FROM departamento inner join municipio on municipio.id_departamento = departamento.id) as tmp on tmp.municipio = hurtos.Municipio
    getAll(result  , row = "*") {
        let self = this;
        let select = this.select(row);
        if ( this.countGroup !== "" )
            select += `, ${this.countGroup} `
        let query = ` SELECT ${select} FROM ${this.table} WHERE 1 = 1 ${this.whereSQL}   `;
        if ( this.group !== "" )
            query += ` GROUP BY ${this.group} `;
        console.log(query)
        this.connection.query( query , function ( err, rows, fields )  {
            if ( err ) throw err;
            // self.connection.end();
            result.json(rows);
        });
    }
    where ( valids ) {
        let whereFinal = '';
        valids.forEach( valid => {
            let item = Object.entries (valid)[0]
            whereFinal += ` AND ${item[0]} = '${item[1]}' `;

        });
        this.whereSQL = whereFinal;
        return this;
    }

    select ( rows ) {
        if ( typeof rows === "object" )
            return rows.join(",")
        else
            return rows;
    }

    groupBy ( groups ) {
        if ( typeof groups === "object" ) {
            this.group = groups.join(",")
            this.countGroup = groups.map( ( group) => {
                return  ` count(${group}) as ${group}_count `;
            } ).join(",")
        }
        else {
            this.group = groups;
            this.countGroup = ` count(${group}) as ${group}_count `;
        }
        return this;
    }

}

module.exports = connect;