let connect = require("./connect");

class MotosModel extends connect {
    table = 'motos';
}

module.exports = MotosModel;