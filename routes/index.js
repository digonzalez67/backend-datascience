var DepartamentosModel = require("../model/DepartamentosModel");
var MunicipioModel = require("../model/MunicipioModel");
var HurtosModel = require("../model/HurtosModel");
var MotosModel = require("../model/MotosModel");

var express = require('express');
var router = express.Router();
let Departamento = new DepartamentosModel();
let Municipio = new MunicipioModel();
let Hurtos = new HurtosModel();
let Motos = new MotosModel();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/departamento', function(req, res, next) {
    Departamento.getAll(res);
});
router.get('/municipio/:departamento', function(req, res, next) {
    Municipio.where( [{ 'id_departamento' : req.params.departamento } ] ).getAll(res  , ['id' , 'nombre'] );
});
router.get('/hurtos/:departamento/:municipio', function(req, res, next) {
    Motos.where( [{ 'id_departamento' : req.params.departamento } , { 'id_municipio' : req.params.municipio } ] ).getAll(res);
    // Hurtos.where( [{ 'departamento' : req.params.departamento } , { 'municipio' : req.params.municipio } ] ).getAll(res);
});
router.get('/hurtos/:departamento/:municipio/:group', function(req, res, next) {
    Motos.where( [{ 'id_departamento' : req.params.departamento } , { 'id_municipio' : req.params.municipio } ] ).groupBy( [ req.params.group ] ).getAll( res, [ req.params.group ]);
    // Hurtos.where( [{ 'departamento' : req.params.departamento } , { 'municipio' : req.params.municipio } ] ).getAll(res);
});

module.exports = router;
